$(function(){

 // MOBILE MENU
  $('.m-nav__btn').click(function(){
    $('.m-nav__btn').toggleClass('m-nav__btn-open');
  });

// select-header
  $('select').selectric();

// slider-top
  $('.slider-top').slick({
    arrows: false,
    fade: true,
    speed: 2000,
    autoplay: true
  });

  var sel = $('.scroll__horizontal');

  sel.each(function()
  {
    var itemsBox = $(this).find('.items__box');

    itemsBox.width(sel.find('.item').outerWidth(true) * itemsBox.find('.item').size());

    $(this).customScrollbar({
        skin: 'interflora-skin',
        fixedThumbWidth: 24,
        fixedViewportHeight: 400
    });
  });

// counter
	$('.calculator__counter').myCounter({
	  step: 1,
	  min: 1,
	  max: 999
	});


// slider-bar
 // Slider Standard sidebar ---------------------------------------------------     
 var Option_SB1_st = 'genoption_sidebar_width_sb1_stand', //ID option 
   Option_SB2_st = 'genoption_sidebar_width_sb2_stand', //ID option 
   Option_SB1_value_st = '20'; //Value Numer id

 var Slider_box_st = $('.sb-width-slider-standard'),
   Slider_barS_st = $('.sliderrange-S-standard'),
   Slider_input1_cont_st = $('.sb-width-slider-standard .content-inputsd-1'),
   Slider_input2_cont_st = $('.sb-width-slider-standard .content-inputsd-2'),
   Slider_input1_st = $('#' + Option_SB1_st),
   Slider_input2_st = $('#' + Option_SB2_st);
 //---------------------------------------------------------------------------  

 //SLIDER JQUERY  -------------------------------------------------------
 function Slider_single(BarS, S_Input1, S_Value1) {
   BarS.slider({
     animate: "fast",
     // orientation: "vertical",
     range: 'min',
     min: 0,
     max: 100,
     step: 1,
     value: S_Value1,
     //values: [ IDoptionSB1_value, IDoptionSB2_value ], // values: [ 30, 99 ],
     slide: function(event, ui) {
       S_Input1.val(ui.value);
     }
   });
 }
 // ----------------------------
 function Slider_multi2(BarR, S_Input1, S_Input2, S_Value1, S_Value2) {
   BarR.slider({
     animate: "fast",
     // orientation: "vertical",
     range: true,
     min: 0,
     max: 100,
     step: 1,
     // value: 10,
     values: [S_Value1, 100 - S_Value2], // values: [ 30, 99 ],
     slide: function(event, ui) {
       S_Input1.val(ui.values[0]);
       S_Input2.val(100 - ui.values[1]);
     }
   });
 } // ----------------------------

 function Slider_single_activation(BarS, S_Input1) {
   S_Input1.val(BarS.slider("value"));
 }

 function Slider_multi2_activation(BarR, S_Input1, S_Input2) {
   S_Input1.val(BarR.slider("values", 0));
   S_Input2.val(100 - BarR.slider("values", 1));
 }

 function Slider_change(BarS, BarR, S_Input1, S_Input2) {
   BarR.slider("values", [S_Input1.val(), (100 - S_Input2.val())]);
   BarS.slider("value", S_Input1.val());
 }

 //ACtion slider -------------------------
 //slider standard
 // Slider_single(Slider_barS_st, Slider_input1_st, Option_SB1_value_st)
 //Slider_multi2(Slider_barR_st, Slider_input1_st, Slider_input2_st, Option_SB1_value_st, Option_SB2_value_st)

 //start Page ------------------------------------------
 //slider standard
 Slider_single_activation(Slider_barS_st, Slider_input1_st);
 //Slider_multi2_activation(Slider_barR_st, Slider_input1_st, Slider_input2_st);

 //to Input on Slider ----------------------------------
 //slider standard
 Slider_input1_st.keyup(function() {
   Slider_change(Slider_barS_st, Slider_barR_st, Slider_input1_st, Slider_input2_st)
 });
 Slider_input2_st.keyup(function() {
   Slider_change(Slider_barS_st, Slider_barR_st, Slider_input1_st, Slider_input2_st)
 });
 Slider_input1_st.change(function() {
   Slider_change(Slider_barS_st, Slider_barR_st, Slider_input1_st, Slider_input2_st)
 });
 Slider_input2_st.change(function() {
   Slider_change(Slider_barS_st, Slider_barR_st, Slider_input1_st, Slider_input2_st)
 });




});